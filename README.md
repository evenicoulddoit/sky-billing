# Sky Billing Assignment
This small project showcases a sample Sky Billing Report using TDD, Django and
AngularJS.

It consists of a single page where a user is instructed to enter their billing date. When the user enters a date, the public API is contacted which simply proxies a [single resource](http://safe-plains-5453.herokuapp.com/bill.json) for a bill dated 2015-01-11. *Enter this date to be presented with the data*.

The styling for the app is quite rough and could easily be improved by using
a CSS pre-processor such as Sass or Less. However, it should support smaller
viewports as the billing components stack up under 960px.

## Installation
* Install Python and [Pip](https://pip.pypa.io/en/latest/installing.html) if required
* Install Python packages: `pip install -Ur requirements.txt`
* Install [NPM](https://www.npmjs.com/) if required and JS test requirements: `npm install`
* Install [Bower](http://bower.io/) if required and JS production requirements `bower install`
* *(Not required)*: Set up the project's database:
```bash
cd billing
python manage.py migrate
```

## Tests
* To run the JS unit tests using Karma, Jasmine and Chrome:
```bash
./node_modules/.bin/karma start
```

* To run the Python unit tests:
```bash
$ cd billing
$ python manage.py test
```

## Running
* To start the app, simply run the development server on localhost port 8000:
```bash
$ cd billing
$ python manage.py runserver
```

## Standards
* An `.editorconfig` file is provided
* All Python should conform to Pep8
* All JS should meet the JSHint requirements (see `.jshintrc`)

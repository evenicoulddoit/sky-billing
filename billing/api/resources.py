import requests


API_ENDPOINT = "http://safe-plains-5453.herokuapp.com/bill.json"


def get_by_date(date):
    """
    Get the report for the current user by date.

    Currently this simply pulls from a static resource, and so we perform some
    additional validation on the response to ensure that it matches the
    requested date.

    Args:
        date (str): A YYYY-MM-DD representation of the start date for the bill.

    Returns:
        The parsed JSON from the API.
        If the API cannot be reached, or no matching bill is found,
        None is returned instead.
    """
    try:
        response = requests.get(API_ENDPOINT)
    except requests.RequestException:
        return None
    try:
        json = response.json()
    except ValueError:
        return None
    else:
        # Perform additional validation to ensure that our billing date matches
        if json.get("statement", {}).get("generated") == date:
            return json
        else:
            return None

from django.conf.urls import url

from .views import APIByDateView


urlpatterns = [
    url(r"^date/(?P<period>\d{4}-\d{2}-\d{2})/$", APIByDateView.as_view())
]

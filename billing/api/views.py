from django.http import JsonResponse
from django.views.generic import DetailView

from . import resources


class APIByDateView(DetailView):
    """
    View which returns the billing data for the given period as JSON.
    """
    http_method_names = [u"get"]
    pk_url_kwarg = "period"

    def get(self, *args, **kwargs):
        bill = resources.get_by_date(self.kwargs[self.pk_url_kwarg])
        if bill is None:
            response = JsonResponse({"error": "Bill not found."})
            response.status_code = 404
            return response
        else:
            return JsonResponse(bill)

"""
Unit tests for the API public views.
"""
from django.test import TestCase, RequestFactory
from mock import patch

from api.views import APIByDateView


@patch("api.resources.get_by_date")
class ApiByDateViewTestCase(TestCase):
    """
    Unit tests for the APIByDateView view.
    """
    def setUp(self):
        self.factory = RequestFactory()

    def test_proxies_get_by_date(self, get_by_date):
        """
        Test that when billing data is available, it is proxied by the view.
        """
        get_by_date.return_value = {"foo": "bar"}

        view = APIByDateView.as_view()
        request = self.factory.get("/")
        response = view(request, period="2012-01-01")

        get_by_date.assert_called_with("2012-01-01")
        self.assertEqual('{"foo": "bar"}', response.content)
        self.assertEqual(200, response.status_code)

    def test_error_shown_when_not_found(self, get_by_date):
        """
        Test that when no data is found, an error message is returned.
        """
        get_by_date.return_value = None

        view = APIByDateView.as_view()
        request = self.factory.get("/")
        response = view(request, period="2012-01-02")

        get_by_date.assert_called_with("2012-01-02")
        self.assertEqual('{"error": "Bill not found."}', response.content)
        self.assertEqual(404, response.status_code)

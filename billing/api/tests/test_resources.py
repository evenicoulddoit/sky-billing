"""
Unit tests for the API Resources.
"""
from django.test import TestCase
from mock import patch, Mock
from requests import RequestException

from api import resources


def mock_bill(date):
    """
    Return a mock bill with the given generation date.
    """
    return {"statement": {"generated": date}}


@patch("api.resources.API_ENDPOINT", new="endpoint/")
@patch("api.resources.requests.get")
class GetByDateTestCase(TestCase):
    """
    Unit tests for the get_by_date API method.
    """
    def test_returns_parsed_json(self, mock_request_get):
        """
        Test that a request to the API is made and the response is parsed.
        """
        matching_data = mock_bill("2000-01-01")
        mock_response = Mock()
        mock_response.json.return_value = matching_data
        mock_request_get.return_value = mock_response

        response = resources.get_by_date("2000-01-01")
        mock_request_get.assert_called_with("endpoint/")
        self.assertEqual(1, mock_response.json.call_count)
        self.assertEqual(matching_data, response)

    def test_invalid_date(self, mock_request_get):
        """
        Test that the request data is verified from the JSON response.
        """
        mock_response = Mock()
        mock_response.json.return_value = mock_bill("1999-12-12")
        mock_request_get.return_value = mock_response

        response = resources.get_by_date("2000-01-01")
        mock_request_get.assert_called_with("endpoint/")
        self.assertEqual(1, mock_response.json.call_count)
        self.assertIsNone(response)

    def test_non_json_returns_none(self, mock_request_get):
        """
        Test that when the API returns non-JSON, the method returns None.
        """
        mock_response = Mock()
        mock_response.json.side_effect = ValueError()
        mock_request_get.return_value = mock_response

        response = resources.get_by_date("2001-10-02")
        mock_request_get.assert_called_with("endpoint/")
        self.assertEqual(1, mock_response.json.call_count)
        self.assertIsNone(response)

    def test_request_exception_returns_none(self, mock_request_get):
        """
        Test that when a RequestException is raised, the method returns None.
        """
        mock_request_get.side_effect = RequestException()
        response = resources.get_by_date("1970-01-01")

        mock_request_get.assert_called_with("endpoint/")
        self.assertIsNone(response)

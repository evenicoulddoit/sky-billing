from django.views.generic import TemplateView


class BillingReportView(TemplateView):
    """
    A very basic view to handle showing a bill report for a given date.
    """
    template_name = "report/index.html"

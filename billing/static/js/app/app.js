/**
 * Create the Sky Billing App and expose it on the window.
 */
(function() {
  "use strict";
  var app = angular.module("skyBillingApp", []);
  window.app = app;
})();

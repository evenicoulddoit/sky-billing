/**
 * Report Controller
 * =============================================================================
 * Adds functionality to load in reports by date.
 * @param {Object} app - App to attach the controller to
 */
(function(app) {
  "use strict";

  app.controller("ReportController", function($scope, $http) {
    var REPORT_ENDPOINT = "/api/date/",
        RE_VALID_DATE = /^\d{4}-\d{2}-\d{2}$/,
        ENTER_KEY = 13;

    $scope.state = "initial";

    /**
     * Called on any keyup event within the date input.
     * If the enter key is pressed, trigger a report request.
     * Reset the state unless currently showing a bill.
     */
    $scope.inputUpdate = function($event) {
      if($event.keyCode == ENTER_KEY) {
        $scope.getReport();
      }
      else if($scope.state != "visible") {
        $scope.state = "initial";
      }
    };

    /**
     * Attempt to retrieve a report for the given date.
     * If the date is not in the YYYY-MM-DD format, show an error state.
     */
    $scope.getReport = function() {
      var isValidDate = !!$scope.date.match(RE_VALID_DATE),
          promise;

      $scope.bill = undefined;

      if(isValidDate) {
        $scope.state = "loading";
        promise = $http.get(REPORT_ENDPOINT + $scope.date);
        promise.then($scope.showReport, $scope.reportUnavailable);
      }

      else {
        $scope.state = "invalid";
      }
    };

    /**
     * Show the report
     */
    $scope.showReport = function(response) {
      $scope.bill = response.data;
      $scope.state = "visible";
    };

    /**
     * Show a message stating that no report was found.
     */
    $scope.reportUnavailable = function() {
      $scope.state = "unavailable";
    };
  });
})(window.app);

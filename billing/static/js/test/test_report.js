/**
 * Unit tests for the report controller.
 */
(function() {
  "use strict";

  describe("ReportController", function() {
    beforeEach(module("skyBillingApp"));

    var $controller, $httpBackend;

    beforeEach(inject(function(_$controller_, _$httpBackend_){
      $controller = _$controller_;
      $httpBackend = _$httpBackend_;
    }));

    /**
     * $scope.inputUpdate() tests
     */
    describe("$scope.inputUpdate", function() {
      var $scope, controller;

      beforeEach(function() {
        $scope = {};
        controller = $controller("ReportController", { $scope: $scope });
      });

      it("calls getReport() when triggered by an enter keypress", function() {
        spyOn($scope, "getReport");
        $scope.inputUpdate({ keyCode: 13 });
        expect($scope.getReport).toHaveBeenCalled();
      });

      it("resets the state when bill not visible", function() {
        spyOn($scope, "getReport");
        $scope.state = "invalid";
        $scope.inputUpdate({});
        expect($scope.getReport).not.toHaveBeenCalled();
        expect($scope.state).toBe("initial");
      });

      it("leaves the state unchanged when bill is visible", function() {
        spyOn($scope, "getReport");
        $scope.state = "visible";
        $scope.inputUpdate({});
        expect($scope.getReport).not.toHaveBeenCalled();
        expect($scope.state).toBe("visible");
      });
    });

    /**
     * $scope.getReport() tests
     */
    describe("$scope.getReport", function() {
      var $scope, controller;

      beforeEach(function() {
        $scope = {};
        controller = $controller("ReportController", { $scope: $scope });
      });

      it("sets the state to invalid when an invalid date is used", function() {
        $scope.date = "not a valid date";
        $scope.getReport();
        expect($scope.state).toBe("invalid");
      });

      it("calls showReport when a bill is returned by the server", function() {
        var callArgs;

        spyOn($scope, "showReport");
        $scope.date = "2001-01-01";
        $httpBackend.whenGET("/api/date/2001-01-01").respond(200, "data");

        $scope.getReport();
        expect($scope.state).toBe("loading");

        $httpBackend.flush();
        expect($scope.showReport).toHaveBeenCalled();
        callArgs = $scope.showReport.calls.mostRecent().args[0];
        expect(callArgs.data).toBe("data");
      });

      it("calls reportUnavailable when no bill is found", function() {
        spyOn($scope, "reportUnavailable");
        $scope.date = "2001-01-01";
        $httpBackend.whenGET("/api/date/2001-01-01").respond(404);
        $scope.getReport();
        expect($scope.state).toBe("loading");
        $httpBackend.flush();
        expect($scope.reportUnavailable).toHaveBeenCalled();
      });
    });

    /**
     * $scope.showReport() tests
     */
    describe("$scope.showReport", function() {
      var $scope, controller;

      beforeEach(function() {
        $scope = {};
        controller = $controller("ReportController", { $scope: $scope });
      });

      it("adds the response data to the state", function() {
        $scope.showReport({ data: "foo" });
        expect($scope.bill).toBe("foo");
      });

      it("sets the state to visible", function() {
        $scope.showReport({ data: "anything" });
        expect($scope.state).toBe("visible");
      });
    });

    /**
     * $scope.reportUnavailable() tests
     */
    describe("$scope.reportUnavailable", function() {
      var $scope, controller;

      beforeEach(function() {
        $scope = {};
        controller = $controller("ReportController", { $scope: $scope });
      });

      it("sets the state to unavailable", function() {
        $scope.reportUnavailable();
        expect($scope.state).toBe("unavailable");
      });
    });
  });
})();

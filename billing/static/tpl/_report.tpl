<h2 class="heading-secondary">
  Bill for {{ bill.statement.generated }}
</h2>
<span class="bill-total">
  Total: {{ bill.total | currency : "£" }}
</span>

<div class="bill-components">
  <section class="bill-component bill-package">
    <h3 class="heading-tertiary">
      Package
    </h3>
    <span class="component-total">
      Total: {{ bill.package.total | currency : "£" }}
    </span>
    <ul class="component-breakdown">
      <li class="component-line"
          ng-repeat="subscription in bill.package.subscriptions">
        <span class="line-entry line-total">
          {{ subscription.cost | currency : "£" }}
        </span>
        <span class="line-entry line-summary">
          {{ subscription.name }} ({{ subscription.type }})
        </span>
      </li>
    </ul>
  </section>
  <section class="bill-component bill-calls">
    <h3 class="heading-tertiary">
      Calls
    </h3>
    <span class="component-total">
      Total: {{ bill.callCharges.total | currency : "£" }}
    </span>
    <ul class="component-breakdown">
      <li class="component-line"
          ng-repeat="call in bill.callCharges.calls">
        <span class="line-entry line-total">
          {{ call.cost | currency : "£" }}
        </span>
        <span class="line-entry">
          {{ call.called }}
        </span>
        <span class="line-entry">
          Duration: {{ call.duration }}
        </span>
      </li>
    </ul>
  </section>
  <section class="bill-component bill-sky-store">
    <h3 class="heading-tertiary">
      Sky Store
    </h3>
    <span class="component-total">
      Total: {{ bill.skyStore.total | currency : "£" }}
    </span>
    <ul class="component-breakdown">
      <li class="component-line"
          ng-repeat="rental in bill.skyStore.rentals">
        <span class="line-entry line-total">
          {{ rental.cost | currency : "£" }}
        </span>
        <span class="line-entry">
          Rental
        </span>
        <label class="line-entry">
          {{ rental.title }}
        </label>
      </li>
      <li class="component-line"
          ng-repeat="buy in bill.skyStore.buyAndKeep">
        <span class="line-entry line-total">
          {{ buy.cost | currency : "£" }}
        </span>
        <span class="line-entry">
          Buy &amp; Keep
        </span>
        <label class="line-entry">
          {{ buy.title }}
        </label>
      </li>
    </ul>
  </section>
</div>

from django.conf.urls import include, url

from report.views import BillingReportView


urlpatterns = [
    url(r"^$", BillingReportView.as_view(), name="reports"),
    url(r"^api/", include("api.urls")),
]
